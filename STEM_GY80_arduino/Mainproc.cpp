/*
STEM_GY80 by Christopher Hwang
www.educaltion.com
January 2015 V1.0
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version. see <http://www.gnu.org/licenses/>
*/

#include <avr/io.h>

#include "Arduino.h"
#include "config.h"
#include "types.h"
#include "Mainproc.h"
#include "Alarms.h"
#include "EEPROM.h"
#include "IMU.h"
#include "Sensors.h"
#include "Serial.h"
#include "Protocol.h"

#include <avr/pgmspace.h>

uint32_t currentTime = 0;
uint16_t previousTime = 0;
uint16_t cycleTime = 0;     // this is the number in micro second to achieve a full loop.
uint16_t calibratingA = 0;  // the calibration is done in the main loop. Calibrating decreases at each cycle down to 0, then we enter in a normal mode.
uint16_t calibratingB = 0;  // baro calibration = get new ground pressure value
uint16_t calibratingG;

// **************
// gyro+acc IMU
// **************
int16_t gyroZero[3] = {0,0,0};

imu_t imu;

alt_t alt;

att_t att;

flags_struct_t f;

//for log
int16_t  i2c_errors_count = 0;
int16_t  annex650_overrun_count = 0;

// ************************
// EEPROM Layout definition
// ************************
global_conf_t global_conf;

conf_t conf;

#if BARO
  int32_t baroPressure;
  int32_t baroTemperature;
  int32_t baroPressureSum;
#endif

void annexCode() { // this code is excetuted at each loop and won't interfere with control loop if it lasts less than 650 microseconds
  static uint32_t calibratedAccTime;

  #if defined(BUZZER)
    alarmHandler(); // external buzzer routine that handles buzzer events globally now
  #endif

  if ( (calibratingA>0 && ACC ) || (calibratingG>0) ) { // Calibration phase
    LEDPIN_TOGGLE;
  } else {
    if (f.ACC_CALIBRATED) {LEDPIN_OFF;}
  }

  if ( currentTime > calibratedAccTime ) {
    if (! f.SMALL_ANGLES_25) {
      // the multi uses ACC and is not calibrated or is too much inclinated
      f.ACC_CALIBRATED = 0;
      LEDPIN_TOGGLE;
      calibratedAccTime = currentTime + 100000;
    } else {
      f.ACC_CALIBRATED = 1;
    }
  }

  serialCom();
}

void setup() {
  #if defined(PROMINI)
    SerialOpen(0,SERIAL0_COM_SPEED);
  #endif
  #if defined(PROMICRO)
    SerialOpen(1,SERIAL1_COM_SPEED);
  #endif
  #if defined(MEGA)
    SerialOpen(1,SERIAL1_COM_SPEED);
    SerialOpen(2,SERIAL2_COM_SPEED);
    SerialOpen(3,SERIAL3_COM_SPEED);
  #endif
  LEDPIN_PINMODE;
  BUZZERPIN_PINMODE;

  readGlobalSet();

  while(1) {                                                    // check settings integrity
  readEEPROM();                                               // check current setting integrity
    if(global_conf.currentSet == 0) break;                      // all checks is done
    global_conf.currentSet--;                                   // next setting for check
  }
  readGlobalSet();                              // reload global settings for get last profile number
  readEEPROM();                                 // load setting data from last used profile
  blinkLED(2,40,global_conf.currentSet+1);          

  initSensors();

  previousTime = micros();

  calibratingG = 512;
  calibratingB = 200;  // 10 seconds init_delay + 200 * 25 ms = 15 seconds before ground pressure settles

  f.SMALL_ANGLES_25 = 1; // important for gyro only conf
}

// ******** Main Loop *********
void loop () {
  static uint8_t taskOrder=0; // never call all functions in the same loop, to avoid high delay spikes
  if(taskOrder>4) taskOrder-=5;
  switch (taskOrder) {
    case 0:
      taskOrder++;
      #if MAG
        if (Mag_getADC()) break; // max 350 µs (HMC5883) // only break when we actually did something
      #endif
    case 1:
      taskOrder++;
      #if BARO
        if (Baro_update() != 0 ) break;
      #endif
    case 2:
      taskOrder++;
      #if BARO
        if (getEstimatedAltitude() !=0) break;
      #endif    
    case 3:
      taskOrder++;
      #if BARO
        if (getTemperature() != 0) break;
      #endif
    case 4:
      taskOrder++;
      break;
  }
 
  computeIMU();
  // Measure loop rate just afer reading the sensors
  currentTime = micros();
  cycleTime = currentTime - previousTime;
  previousTime = currentTime;
}
