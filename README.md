# STEM-GY80 #

This is the code repository for an Arduino hooked up to a GY-80 IMU used in a STEM curriculum for K12 kids.

#### Credits ####
Code from the MultiWii and MultiWiiConf projects found at http://www.multiwii.com was used in this project.